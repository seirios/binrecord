#include <errno.h>
#include <stdio.h>
#include <stddef.h>
#include <endian.h>
#include <limits.h>

#include "parse.h"

/* Parse BINRECORD format.
 * Grammar is:
 *     [repeat] <data_type> <size_bits> [endianness]
 * where:
 *     repeat, optional with default = 1, a positive decimal integer
 *     data_type, a single character:
 *         _ => ignore (skip)
 *         i => signed integer
 *         u => unsigned integer
 *         f => floating-point number (IEEE 754)
 *     size_bits, one of: 8, 16, 32, 64, or 128
 *     endianness, optional with default = host-endian, one of:
 *         le => little-endian
 *         be => big-endian
 *
 * Example: "5f32 i8 u16le" means
 *        + five floats (host-endian), and
 *        + an 8-bit integer, and
 *        + a 16-bit unsigned integer (little-endian)
 */

/* macros */
#define AVAIL_DIGITS ((int)(sizeof(digit_buf) / sizeof(*digit_buf)))
#define PASS(n) { sp += (n); done = 1; continue; }
#define FAIL(x) { rc = -(10 * state + x); goto end; }
#define ADVANCE(x) { state = (x); ++sp; continue; }
#define ADVANCE_N(x,n) { state = (x); sp += (n); continue; }
#define SWITCH(x) { state = (x); continue; }

/* states */
#define DTYPE 1
#define REPEAT 2
#define SIZEBITS 3
#define ENDIAN 4
#define STORE 5

/* error codes */
#define INCOMPLETE 0
#define BADCHAR 1
#define EXCESSDIGITS 2
#define CONVERSION 3

int parse_binrecord(const char *restrict s, struct bFormat *res_o, char **restrict endptr) {
    static const struct bField fmt_default = { .repeat= 1, .endian= BYTE_ORDER };

    if(!s || !res_o) return EINVAL; // check inputs defensively

    int rc = 0; // return code
    struct bFormat res = { 0 }; // resulting list of bField

    const char *sp = s; // pointer to input string
    char digit_buf[24] = { 0 }; // digit buffer for integer conversion
    struct bField fmt = fmt_default; // parsed field

    /* Skip leading blanks. */
    while(' ' == *sp || '\t' == *sp) ++sp;

    /*
     * Parse input string.
     *   done : exit flag, used to break the loop from inside the switch
     *  state : state of the state machine
     *   nadv : number of characters to advance when changing state
     * idigit : index inside the digit buffer
     */
    for(int done = 0, state = DTYPE, nadv = 1, idigit = 0; !done; ) {
        char c = *sp; // current character
        switch(state) {
            case DTYPE:
                if(   '1' == c || '2' == c || '3' == c || '4' == c
                   || '5' == c || '6' == c || '7' == c || '8' == c || '9' == c) {
                    SWITCH(REPEAT);
                } else if(     '_' == c /* fmt.dtype = DTYPE_SKIP by default */
                          || (('i' == c || 'I' == c) && (fmt.dtype = DTYPE_INT))
                          || (('u' == c || 'U' == c) && (fmt.dtype = DTYPE_UINT))
                          || (('f' == c || 'F' == c) && (fmt.dtype = DTYPE_FP))) {
                    ADVANCE(SIZEBITS);
                } else if('\0' == c) { FAIL(INCOMPLETE); } // incomplete field
                else { FAIL(BADCHAR); } // bad char
                break;
            case REPEAT:
                if(   '0' == c || '1' == c || '2' == c || '3' == c || '4' == c
                   || '5' == c || '6' == c || '7' == c || '8' == c || '9' == c) {
                    if(idigit + 1 < AVAIL_DIGITS) { // if space available, then
                        digit_buf[idigit++] = c;    // collect digit, else
                    } else { FAIL(EXCESSDIGITS); }  // too many digits
                    ADVANCE(REPEAT);
                } else if('\0' == c) { FAIL(INCOMPLETE); } // incomplete field
                else { // no more digits
                    digit_buf[idigit] = '\0'; // NUL-terminate digit buffer
                    errno = 0;
                    long repeat = strtol(digit_buf,NULL,10);
                    if(errno != 0 || repeat <= 0 || repeat > INT_MAX) { FAIL(CONVERSION); } // failed conversion
                    idigit = 0; // reset digit index
                    fmt.repeat = repeat;
                    SWITCH(DTYPE);
                }
                break;
            case SIZEBITS:
                if(   ('8' == c                                         && (fmt.sizebits = 8)   && (nadv = 1))
                   || ('1' == c && '6' == *(sp + 1)                     && (fmt.sizebits = 16)  && (nadv = 2))
                   || ('3' == c && '2' == *(sp + 1)                     && (fmt.sizebits = 32)  && (nadv = 2))
                   || ('6' == c && '4' == *(sp + 1)                     && (fmt.sizebits = 64)  && (nadv = 2))
                   || ('1' == c && '2' == *(sp + 1) && '8' == *(sp + 2) && (fmt.sizebits = 128) && (nadv = 3))) {
                    ADVANCE_N(ENDIAN,nadv);
                } else if('\0' == c) { FAIL(INCOMPLETE); } // incomplete field
                else { FAIL(BADCHAR); } // bad char
                break;
            case ENDIAN:
                if(   ((('l' == c && 'e' == *(sp + 1)) || ('L' == c && 'E' == *(sp + 1))) && (fmt.endian = LITTLE_ENDIAN))
                   || ((('b' == c && 'e' == *(sp + 1)) || ('B' == c && 'E' == *(sp + 1))) && (fmt.endian = BIG_ENDIAN))) {
                    ADVANCE_N(STORE,2);
                } else if(' ' == c || '\t' == c || '\0' == c) {
                    SWITCH(STORE);
                } else { FAIL(BADCHAR); } // bad char
                break;
            case STORE:
                if(   ('\0' == c               && !(nadv = 0))
                   || ((' ' == c || '\t' == c) &&  (nadv = 1))) {
                    /* Skip trailing blanks. */
                    while(' ' == *(sp + nadv) || '\t' == *(sp + nadv)) ++nadv;

                    /* Append to resulting list. */
                    res = bfmt_add(res,&fmt);

                    if('\0' == *(sp + nadv)) { PASS(nadv); } // EOL reached, finish
                    else { // parse next field
                        fmt = fmt_default; // reset field
                        ADVANCE_N(DTYPE,nadv);
                    }
                } else { FAIL(BADCHAR); } // bad char
                break;
        }
    }

    *res_o = res; // store result
    res = (struct bFormat){ 0 }; // hand-over

end:
    res = bfmt_free(res); // cleanup
    if(endptr) *endptr = (char*)sp;

    return rc;
}

#if 1
int main (int argc, char *argv[]) {
    if(argc > 1) {
        const char *str = argv[1];
        struct bFormat bfmt;
        char *endptr = NULL;
        int ret = parse_binrecord(str,&bfmt,&endptr);
        if(ret == 0) {
            size_t nf = 0;
            size_t nbyte = 0;
            for(size_t i = 0; i < bfmt.length; ++i) {
                struct bField *x = &bfmt.list[i];
                nf += (x->dtype == DTYPE_SKIP) ? 0 : x->repeat;
                nbyte += x->repeat * (x->sizebits / 8);
            }
            printf("VALID (NF = %zu, %zu bytes per record)\n",nf,nbyte);
            bfmt = bfmt_free(bfmt);
        } else {
            printf("INVALID (rc = %d at position %jd '%c')\n",
                   ret,(ptrdiff_t)(endptr - str),*endptr);
        }
    } else puts("EMPTY");
}
#endif
