#ifndef PARSE_H_
#define PARSE_H_

#include <string.h>
#include <stdlib.h>

#define BLKSIZE 4

enum bField_dtype {
    DTYPE_SKIP,
    DTYPE_INT,
    DTYPE_UINT,
    DTYPE_FP
};

struct bField {
    enum bField_dtype dtype; // data type
    int repeat;   // number of fields
    int sizebits; // size in bits
    int endian;   // endianness
};

// bFormat is a list of struct bField
struct bFormat {
    struct bField* list;
    size_t length;
    size_t capacity;
};

inline static struct bFormat bfmt_add(struct bFormat x, struct bField *item) {
    void *tmp = NULL;

    switch(x.capacity - x.length) {
        case 0:
            if((tmp = realloc(x.list,(x.capacity += BLKSIZE) * sizeof(*x.list))))
                x.list = tmp; // update
            else {
                exit(EXIT_FAILURE);
            }
            /* fall-through */
        default:
            memcpy(x.list + x.length++,item,sizeof(*item));
    }

    return x;
}

inline static struct bFormat bfmt_free(struct bFormat x) {
    free(x.list); x.list = NULL;
    x.capacity = x.length = 0;
    return x;
}

#endif
