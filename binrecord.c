/*
 * binrecord.c - Use AWK to read binary records.
 *
 * Sirio Bolaños Puchet
 * July 2020
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <stdio.h>
#include <assert.h>
#include <errno.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include <sys/types.h>
#include <sys/stat.h>

#include "gawkapi.h"

#include "gettext.h"
#define _(msgid)  gettext(msgid)
#define N_(msgid) msgid

static const gawk_api_t *api;    /* for convenience macros to work */
static awk_ext_id_t ext_id;
static const char *ext_version = "binrecord extension: version 0.1";
static awk_bool_t (*init_func)(void) = NULL;

int plugin_is_GPL_compatible;

/* Forward function declarations: */
static awk_bool_t can_take_file(const awk_input_buf_t *iop);
static awk_bool_t take_control_of(awk_input_buf_t *iop);

static awk_ext_func_t func_table[] = {
};

dl_load_func(func_table, binrecord, "")

static awk_input_parser_t binrecord_parser = {
    "binrecord",
    can_take_file,
    take_control_of,
    NULL
};

static awk_bool_t can_take_file(const awk_input_buf_t *iop __UNUSED) {
    awk_value_t value;
    /* Check BINRECORD variable for record format */
    if(sym_lookup("BINRECORD", AWK_STRING, &value)) {
        const char *fmtstr = value.str_value.str;

        /* Parse format and check for validity
         * Format is:
         *     [repeat] [data type] [data size] [data endian] [flags]
         * where:
         *     repeat, is a positive integer
         *     data type, is a single character:
         *         i => signed integer
         *         u => unsigned integer
         *         f => floating-point number (IEEE 754)
         *     data size, is one of: 8, 16, 32, 64, or 128
         *     data endian, is one of: le, be
         *     flags, is zero or more characters:
         *         * => 
         */
    
        /* If format is valid, take file */
        return 1;
    }

    return 0;
}

static awk_bool_t take_control_of(awk_input_buf_t *iop) {
    static int warned = FALSE;

    if(do_lint && !warned) {
        warned = TRUE;
        lintwarn(ext_id, _("`BINRECORD' is a gawk extension'"));
    }


    iop->get_record = binrecord_get_record;

    return 1;
}

static int
binrecord_get_record(char **out,      /* pointer to pointer to data */
    awk_input_buf_t *iop,       /* input IOP */
    int *errcode,               /* pointer to error variable */
    char **rt_start __UNUSED,   /* output: pointer to RT */
    size_t *rt_len,             /* output: length of RT */
    const awk_fieldwidth_info_t **field_width __UNUSED
                                /* output: optional field widths */
    ) {
}
