CC := gcc --std=c99
CFLAGS := -Wall -Wextra -Werror -pedantic -pedantic-errors
CFLAGS += -O2
CPPFLAGS := -D_DEFAULT_SOURCE

parse: parse.h
